#!/bin/bash
set -e

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
VERSION="$($DIR/../bin/get-version.sh)"
SYNC_GATEWAY_VERSIONED_TAG="registry.gitlab.com/mpapp-public/couchbase-images/sync_gateway:${VERSION}"
COUCHBASE_VERSIONED_TAG="registry.gitlab.com/mpapp-public/couchbase-images/couchbase:${VERSION}"

if [[ -z "${CI_JOB_TOKEN}" ]]; then
  echo $GITLAB_ACCESS_TOKEN | docker login -u $GITLAB_USER --password-stdin registry.gitlab.com
else
  echo $CI_JOB_TOKEN | docker login -u gitlab-ci-token --password-stdin registry.gitlab.com
fi

echo "Committing, tagging, pushing couchbase…"
cd $DIR/../images/couchbase
docker build -t couchbase .
docker tag couchbase registry.gitlab.com/mpapp-public/couchbase-images/couchbase
docker tag couchbase ${COUCHBASE_VERSIONED_TAG}
docker push registry.gitlab.com/mpapp-public/couchbase-images/couchbase

echo "Committing, tagging, pushing sync_gateway…"
cd $DIR/../images/sync_gateway
docker build \
--build-arg SG_EDITION=CE \
--build-arg SG_FILENAME=sync_gateway_ce \
--build-arg MANUSCRIPTS_SYNC_VERSION="@$VERSION" \
-t sync_gateway .
docker tag sync_gateway registry.gitlab.com/mpapp-public/couchbase-images/sync_gateway
docker tag sync_gateway ${SYNC_GATEWAY_VERSIONED_TAG}
docker push registry.gitlab.com/mpapp-public/couchbase-images/sync_gateway
